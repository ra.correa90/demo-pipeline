#!/bin/bash

APP_DIR="/var/www/demo-pipeline"

# Create the .ssh directory and set permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Add the SSH private key to the file
echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

# Add the remote host to the known_hosts file
ssh-keyscan -H $HOST_NAME >> ~/.ssh/known_hosts

# Deploy the application to the remote server
ssh -i ~/.ssh/id_rsa $HOST_USER@$HOST_NAME "cd $APP_DIR && bash deploy/deploy.sh"