#!/bin/bash

APP_DIR="/var/www/demo-pipeline"
PROCESS_NAME="api"

# Verificar si el directorio de la aplicación existe
if [ ! -d "$APP_DIR" ]; then
  echo "El directorio de la aplicación no existe: $APP_DIR"
  exit 1
fi

# Navegar al directorio de la aplicación
cd "$APP_DIR" || exit 1

# Descartar cambios locales
sudo git reset --hard origin/main
sudo git pull
sudo git checkout main
sudo npm ci

# Detener y eliminar el proceso en PM2 si existe
sudo pm2 describe "$PROCESS_NAME" >/dev/null
(sudo pm2 stop "$PROCESS_NAME" || true)
sudo pm2 delete "$PROCESS_NAME" || true

# Ejecutar la aplicación
if ! sudo npm run start; then
  echo "Error al ejecutar la aplicación"
  exit 1
fi
